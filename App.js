import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Page from './components/Page';
import Login from './components/Login';
import Signup from './components/Signup';
import Flat from './components/Flat';
import Profile from './components/Profile';
import Dash from './components/Dash';
import 'react-native-gesture-handler';
import {createDrawerNavigator} from '@react-navigation/drawer';

import splash from './components/splash';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';

const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createMaterialBottomTabNavigator();

function MyDrawer() {
  return (
    <Drawer.Navigator initialRouteName="page">
      <Drawer.Screen name="Login" component={Login} />
      <Drawer.Screen name="Signup" component={Signup} />
      <Drawer.Screen name="Profile" component={Profile} />
      <Drawer.Screen name="Dash" component={Dash} />
    </Drawer.Navigator>
  );
}
function MyTabs() {
  return (
    
    <Tab.Navigator>
      <Tab.Screen name="Profile" component={Profile} />
      <Tab.Screen name="Dash" component={Dash} />
    </Tab.Navigator>
     
  );
}

export default function () {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="splash">
        <Stack.Screen
          name="splash"
          component={splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Page"
          component={Page}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={MyDrawer}
          
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Signup"
          component={Signup}
          options={{headerShown: false}}
        />

        <Stack.Screen name="Flat" component={Flat} />
        <Stack.Screen name="Profile" component={Profile} />
        <Stack.Screen name="Dash" component={Dash} />
        
      </Stack.Navigator>
      
    </NavigationContainer>
    
  );
}

const styles = StyleSheet.create({});
