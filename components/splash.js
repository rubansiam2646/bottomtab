
import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';

const splash = ({navigation}) => {
  React.useEffect(() => {
    
    setTimeout(() => {
      navigation.navigate('Page');
    }, 3000);
  }, []);

  return (
    <View style={styles.container}>
      <Image source={require('../assests/splash.jpg')} />
    </View>
  );
};

export default splash;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'pink',
  },
});
